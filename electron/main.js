const { app, BrowserWindow, Menu, screen } = require('electron')
const path = require("path")
const isPackaged = app.isPackaged;

Menu.setApplicationMenu(null);

let mainWindow;
const createWindow = () => {
    mainWindow = new BrowserWindow({
        title: "easy-electron",
        width: screen.getPrimaryDisplay().workAreaSize.width,
        height: screen.getPrimaryDisplay().workAreaSize.height,
        minWidth: 800,
        minHeight: 600,
        icon: path.resolve(__dirname, "../build/icon.ico")
    })

    if (!isPackaged) {
        mainWindow.webContents.openDevTools();
    }

    // 主要改了这里
    // mainWindow.loadFile(path.join(__dirname, "./index.html"));
    // 使用 loadURL 加载 http://localhost:3004 ，也就是我们刚才创建的 Vue 项目地址
    // 3004 改为你 Vue 项目的端口号
    mainWindow.loadURL("http://localhost:5173/");
    // mainWindow.loadURL(`file://${path.join(__dirname, "../dist/index.html")}`);
}

app.whenReady().then(() => {
    createWindow()
    app.on("activate", () => {
        // 通常在 macOS 上，当点击 dock 中的应用程序图标时，如果没有其他
        // 打开的窗口，那么程序会重新创建一个窗口。
        if (BrowserWindow.getAllWindows().length === 0) createWindow();
    });
})

// 除了 macOS 外，当所有窗口都被关闭的时候退出程序。 因此，通常对程序和它们在任务栏上的图标来说，应当保持活跃状态，直到用户使用 Cmd + Q 退出。
app.on("window-all-closed", () => {
    if (process.platform !== "darwin") app.quit();
});

// 如果开发环境使用了 nginx 代理，禁止证书报错
if (!isPackaged) {
    // 证书的链接验证失败时，触发该事件
    app.on(
        "certificate-error",
        function (event, webContents, url, error, certificate, callback) {
            event.preventDefault();
            callback(true);
        }
    );
}